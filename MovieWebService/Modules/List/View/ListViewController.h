//
//  ViewController.h
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListProtocols.h"
#import "BaseViewController.h"

@interface ListViewController : BaseViewController <ListViewInterface> 

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) id<ListModuleInterface> presenter;

@end
