//
//  ViewController.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListViewController.h"
#import "DetailViewController.h"
#import "MovieCell.h"
#import "Film.h"

@interface ListViewController() <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter viewDidLoad];
    [self config];
}

- (void)config {
    self.navigationItem.title = @"Root View Controller";
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    UINib *cellNib = [UINib nibWithNibName:@"MovieCell" bundle:nil];
    [_tableView registerNib:cellNib forCellReuseIdentifier:@"MovieCell"];
}

#pragma mark - ListViewInterface methods

- (void)reloadView {
    [_tableView reloadData];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter filmCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MovieCell";
    MovieCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell fillWith:[self.presenter cellInfoAtIndex:indexPath.row]];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.presenter showDetail:[self.presenter cellInfoAtIndex:indexPath.row]];
}

@end
