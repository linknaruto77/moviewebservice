//
//  MovieCell.h
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>

@class Film;

@interface MovieCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *name;
@property (nonatomic, strong) IBOutlet UILabel *date;
@property (nonatomic, strong) IBOutlet UILabel *filmRating;
@property (nonatomic, strong) IBOutlet UILabel *rating;

- (void)fillWith:(Film *)cellInfo;

@end
