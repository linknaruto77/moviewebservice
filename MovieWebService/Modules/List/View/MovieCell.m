//
//  MovieCell.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "MovieCell.h"
#import "Film.h"

@interface MovieCell()

@end

@implementation MovieCell

- (void)fillWith:(Film *)cellInfo {
    _name.text = cellInfo.name;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setCalendar:[NSCalendar new]];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    _date.text = [dateFormatter stringFromDate:cellInfo.releaseDate];
    
    NSString *filmRatingText;
    switch (cellInfo.filmRating) {
        case G:
            filmRatingText = @"G";
        case PG:
            filmRatingText = @"PG";
        case PG13:
            filmRatingText = @"PG13";
        case R:
            filmRatingText = @"R";
        default:
            break;
    }
    _filmRating.text = filmRatingText;
    _rating.text = [NSString stringWithFormat:@"%0.1f", cellInfo.rating];
}

@end
