//
//  ListWireframe.h
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>

@class ListPresenter;
@class Film;

@interface ListWireframe : NSObject

@property (nonatomic, weak) ListPresenter *presenter;

+ (UIViewController *)assembleModule;
- (void)presentDetailInterface:(Film *)film;

@end
