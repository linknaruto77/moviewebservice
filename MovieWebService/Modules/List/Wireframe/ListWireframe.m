//
//  ListWireframe.m
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListWireframe.h"
#import "ListViewController.h"
#import "ListInteractor.h"
#import "ListPresenter.h"
#import "DetailWireframe.h"
#import "ListDataManager.h"
#import "Film.h"

@interface ListWireframe ()

@property (nonatomic, strong) ListViewController *viewController;

@end

@implementation ListWireframe

+ (UIViewController *)assembleModule {
    ListViewController *vc = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    ListPresenter *presenter = [[ListPresenter alloc] init];
    ListInteractor *interactor = [[ListInteractor alloc] init];
    ListWireframe *wireFrame = [[ListWireframe alloc] init];
    ListDataManager *listDataManager = [[ListDataManager alloc] init];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    vc.presenter = presenter;
    wireFrame.presenter = presenter;
    wireFrame.viewController = vc;
    presenter.userInterface = vc;
    presenter.interactor = interactor;
    presenter.wireframe = wireFrame;
    interactor.presenter = presenter;
    interactor.dataManager = listDataManager;
    
    return nav;
}

- (void)presentDetailInterface:(Film *)film {
    UIViewController *vc = [DetailWireframe assembleModule:film];
    [self.viewController.navigationController pushViewController:vc animated:YES];
}

@end
