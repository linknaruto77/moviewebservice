//
//  ListModuleInterface.h
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>

@class Film;

// VIEW -> PRESENTER
@protocol ListModuleInterface <NSObject>
- (void)viewDidLoad;
- (NSUInteger)filmCount;
- (Film *)cellInfoAtIndex:(NSInteger)index;
- (void)showDetail:(id)film;
@end

// PRESENTER -> VIEW
@protocol ListViewInterface <NSObject>
- (void)reloadView;
@end

@protocol ListModuleDelegate <NSObject>

@end

// PRESENTER -> INTERACTOR
@protocol ListInteractorInputProtocol <NSObject>
- (NSArray *)retrieveMovieList;
@end

// INTERACTOR -> PRESENTER
@protocol ListInteractorOutputProtocol <NSObject>
- (void)didRetrieveFilms:(NSArray *)films;
- (void)onError;
@end

