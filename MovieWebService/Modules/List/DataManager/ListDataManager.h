//
//  ListDataManager.h
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListDataManager : NSObject

- (NSArray *)retrieveMovieList;

@end
