//
//  ListInteractor.h
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListProtocols.h"

@class ListDataManager;

@interface ListInteractor : NSObject <ListInteractorInputProtocol>

@property (nonatomic, weak) id<ListInteractorOutputProtocol> presenter;
@property (nonatomic, strong) ListDataManager *dataManager;

- (void)retrieveMovieList;

@end
