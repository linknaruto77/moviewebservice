//
//  ListInteractor.m
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListInteractor.h"
#import "ListPresenter.h"
#import "ListDataManager.h"
#import "Film.h"

@implementation ListInteractor

- (void)retrieveMovieList {
    @try {
        NSArray *movieList = [self.dataManager retrieveMovieList];
        NSMutableArray *result = [NSMutableArray arrayWithCapacity:movieList.count];
        for (NSDictionary *movie in movieList) {
            Film *film = [[Film alloc] initWithData:movie];
            [result addObject:film];
        }
        [self.presenter didRetrieveFilms:result];
    } @catch (NSException *exception) {
        [self.presenter onError];
    }
}

@end
