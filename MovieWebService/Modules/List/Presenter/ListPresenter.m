//
//  ListPresenter.m
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "ListPresenter.h"
#import "ListInteractor.h"
#import "ListWireframe.h"
#import "Film.h"

@interface ListPresenter() {
    NSArray *filmList;
}

@end

@implementation ListPresenter

- (NSUInteger)filmCount {
    return filmList.count;
}

- (Film *)cellInfoAtIndex:(NSInteger)index {
    return filmList[index];
}

#pragma mark - ListModuleInterface methods

- (void)viewDidLoad {
    [self.interactor retrieveMovieList];
}

- (void)showDetail:(id)film {
    [self.wireframe presentDetailInterface:film];
}

#pragma mark - ListInteractorOutputProtocol methods

- (void)didRetrieveFilms:(NSArray *)films {
    filmList = films;
    [self.userInterface reloadView];
}

- (void)onError {
    
}


@end
