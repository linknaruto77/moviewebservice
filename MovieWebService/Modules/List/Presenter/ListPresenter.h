//
//  ListPresenter.h
//  MovieWebService
//
//  Created by Chung on 28/03/17.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListProtocols.h"

@class ListWireframe;
@class Film;

@interface ListPresenter : NSObject <ListModuleInterface, ListInteractorOutputProtocol>

@property (nonatomic, strong) ListWireframe *wireframe;

@property (nonatomic, strong) id<ListInteractorInputProtocol> interactor;
@property (nonatomic, weak) UIViewController<ListViewInterface> *userInterface;

- (NSUInteger)filmCount;
- (Film *)cellInfoAtIndex:(NSInteger)index;

@end
