//
//  DirectorCell.m
//  MovieWebService
//
//  Created by Chung on 31/03/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import "DirectorCell.h"
#import "Director.h"

@interface DirectorCell()

@end

@implementation DirectorCell

- (void)fillWith:(Director *)cellInfo {
    _name.text = cellInfo.name;
}

@end
