//
//  CastCell.m
//  MovieWebService
//
//  Created by Chung on 31/03/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import "CastCell.h"
#import "Actor.h"

@interface CastCell()

@end

@implementation CastCell

- (void)fillWith:(Actor *)cellInfo {
    _name.text = cellInfo.name;
    _screeName.text = cellInfo.screenName;
}

@end
