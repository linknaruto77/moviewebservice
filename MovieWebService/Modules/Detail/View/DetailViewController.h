//
//  DetailViewController.h
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DetailProtocols.h"

@class Film;

@interface DetailViewController : BaseViewController

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) id<DetailModuleInterface> presenter;

@end
