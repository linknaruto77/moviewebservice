//
//  DetailViewController.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "DetailViewController.h"
#import "DetailProtocols.h"
#import "DirectorCell.h"
#import "CastCell.h"
#import "MovieCell.h"
#import "Film.h"
#import "Director.h"
#import "Actor.h"

@interface DetailViewController () <DetailViewInterface, UITableViewDataSource, UITableViewDelegate> {
    NSArray *castList;
    BOOL showMore;
}

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_presenter viewDidLoad];
    [self config];
}

- (void)config {
    showMore = true;
    _tableView.estimatedRowHeight = 100;
    _tableView.rowHeight = UITableViewAutomaticDimension;
    
    NSArray *nibNames = @[@"CastCell", @"DirectorCell"];
    for (NSString *name in nibNames) {
        UINib *cellNib = [UINib nibWithNibName:name bundle:nil];
        [_tableView registerNib:cellNib forCellReuseIdentifier:name];
    }
}

#pragma mark - IBActions

- (IBAction)didTapOnShowMore:(id)sender {
    showMore = false;
    [_tableView reloadData];
}

#pragma mark - DetailViewInterface methods

- (void)reloadView {
    [_tableView reloadData];
}

#pragma mark - Button event handlers

// ** handle UI events here

#pragma mark - UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return showMore ? 0 : [self.presenter actorCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"CastCell";
    CastCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell fillWith:[self.presenter cellInfoAtIndex:indexPath.row]];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return showMore ? 112 : 81;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"DirectorCell";
    DirectorCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell fillWith:self.presenter.film.director];
    return cell.contentView;
}

@end
