//
//  CastCell.h
//  MovieWebService
//
//  Created by Chung on 31/03/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>

@class Actor;

@interface CastCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *name;
@property (nonatomic, strong) IBOutlet UILabel *screeName;

- (void)fillWith:(Actor *)actor;

@end
