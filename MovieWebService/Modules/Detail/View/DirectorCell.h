//
//  DirectorCell.h
//  MovieWebService
//
//  Created by Chung on 31/03/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>

@class Director;

@interface DirectorCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *name;

- (void)fillWith:(Director *)cellInfo;

@end
