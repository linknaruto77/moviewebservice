//
//  DetailInteractor.h
//  MovieWebService
//
//  Created by Chung on 29/03/17.
//
//

#import <Foundation/Foundation.h>
#import "DetailProtocols.h"

@class DetailDataManager;

@interface DetailInteractor : NSObject <DetailInteractorInputProtocol>

@property (nonatomic, weak) id<DetailInteractorOutputProtocol> presenter;
@property (nonatomic, strong) DetailDataManager *dataManager;

@end
