//
//  DetailPresenter.m
//  MovieWebService
//
//  Created by Chung on 29/03/17.
//
//

#import "DetailPresenter.h"
#import "Film.h"

@implementation DetailPresenter

- (NSUInteger)actorCount {
    return _film.cast.count;
}

- (Actor *)cellInfoAtIndex:(NSInteger)index {
    return _film.cast[index];
}

#pragma mark - DetailModuleInterface methods

- (void)viewDidLoad {
    [_userInterface reloadView];
}

@end
