//
//  DetailPresenter.h
//  MovieWebService
//
//  Created by Chung on 29/03/17.
//
//

#import <Foundation/Foundation.h>
#import "DetailProtocols.h"
#import "DetailInteractor.h"
#import "DetailWireframe.h"

@class DetailWireframe;
@class Film;

@interface DetailPresenter : NSObject <DetailModuleInterface, DetailInteractorOutputProtocol>

@property (nonatomic, strong)  id<DetailInteractorInputProtocol> interactor;
@property (nonatomic, weak) DetailWireframe *wireframe;
@property (nonatomic, strong) Film *film;

@property (nonatomic, weak) UIViewController<DetailViewInterface> *userInterface;

@end
