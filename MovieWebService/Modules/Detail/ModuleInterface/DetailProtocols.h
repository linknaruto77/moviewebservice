//
//  DetailModuleInterface.h
//  MovieWebService
//
//  Created by Chung on 29/03/17.
//
//

#import <Foundation/Foundation.h>

@class Film;
@class Actor;

// VIEW -> PRESENTER
@protocol DetailModuleInterface <NSObject>
@property (nonatomic, strong) Film *film;
- (void)viewDidLoad;
- (NSUInteger)actorCount;
- (Actor *)cellInfoAtIndex:(NSInteger)index;
@end

// PRESENTER -> VIEW
@protocol DetailViewInterface <NSObject>
- (void)reloadView;
@end

@protocol DetailModuleDelegate <NSObject>

@end

// PRESENTER -> INTERACTOR
@protocol DetailInteractorInputProtocol <NSObject>

@end

// INTERACTOR -> PRESENTER
@protocol DetailInteractorOutputProtocol <NSObject>

@end



