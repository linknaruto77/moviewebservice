//
//  DetailWireframe.m
//  MovieWebService
//
//  Created by Chung on 29/03/17.
//
//

#import "DetailWireframe.h"
#import "DetailViewController.h"
#import "DetailInteractor.h"
#import "DetailPresenter.h"
#import "DetailWireframe.h"
#import "Film.h"

@interface DetailWireframe ()

@property (nonatomic, strong) DetailViewController *viewController;

@end

@implementation DetailWireframe

+ (UIViewController *)assembleModule:(Film *)film {
    DetailViewController *vc = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    DetailPresenter *presenter = [[DetailPresenter alloc] init];
    DetailInteractor *interactor = [[DetailInteractor alloc] init];
    DetailWireframe *wireFrame = [[DetailWireframe alloc] init];
    
    presenter.film = film;
    
    vc.presenter = presenter;
    wireFrame.viewController = vc;
    wireFrame.presenter = presenter;
    presenter.userInterface = vc;
    presenter.interactor = interactor;
    presenter.wireframe = wireFrame;
    interactor.presenter = presenter;
    
    
    return vc;
}

@end
