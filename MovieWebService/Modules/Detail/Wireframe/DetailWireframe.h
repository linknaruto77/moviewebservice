//
//  DetailWireframe.h
//  MovieWebService
//
//  Created by Chung on 29/03/17.
//
//

#import <Foundation/Foundation.h>
#import "DetailPresenter.h"

@class DetailPresenter;
@class Film;

@interface DetailWireframe : NSObject

@property (nonatomic, strong) DetailPresenter *presenter;

+ (UIViewController *)assembleModule:(Film *)film;

@end
