//
//  RootWireFrame.h
//  MovieWebService
//
//  Created by Chung on 06/04/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RootProtocols.h"

@interface RootWireFrame : NSObject <RootModuleInterface>

@end
