//
//  RootProtocols.h
//  MovieWebService
//
//  Created by Chung on 06/04/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//


@protocol RootModuleInterface <NSObject>

- (void)presentListScreen:(UIWindow *)window;

@end
