//
//  RootWireFrame.m
//  MovieWebService
//
//  Created by Chung on 06/04/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import "RootWireFrame.h"
#import "ListWireframe.h"

@implementation RootWireFrame

- (void)presentListScreen:(UIWindow *)window {
    [window makeKeyAndVisible];
    window.rootViewController = [ListWireframe assembleModule];
}

@end
