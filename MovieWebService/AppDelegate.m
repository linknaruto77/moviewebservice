//
//  AppDelegate.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "AppDelegate.h"
#import "RootWireFrame.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    RootWireFrame *wireFrame = [[RootWireFrame alloc] init];
    [wireFrame presentListScreen:_window];
    
    return YES;
}
    
@end
