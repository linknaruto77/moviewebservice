//
//  Film.m
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import "Film.h"
#import "Actor.h"
#import "Director.h"

@implementation Film

- (id)initWithData:(NSDictionary *)data {
    self = [super init];
    if (self) {
        self.filmRating = [data[@"filmRating"] doubleValue];
        self.languages = data[@"languages"];
        self.nominated = [data[@"nominated"] boolValue];
        self.releaseDate = [NSDate dateWithTimeIntervalSince1970:[data[@"releaseDate"] doubleValue]];
        self.name = data[@"name"];
        self.rating = [data[@"rating"] doubleValue];
        self.director = [[Director alloc] initWithData:data[@"director"]];
        
        NSArray *castsData = data[@"cast"];
        NSMutableArray *castsList = [NSMutableArray arrayWithCapacity:castsData.count];
        for (NSDictionary *castData in castsData) {
            Actor *actor = [[Actor alloc] initWithData:castData];
            actor.film = self;
            [castsList addObject:actor];
        }
        self.cast = castsList;
    }
    return self;
}

@end
