//
//  Film.h
//  MovieWebService
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <Foundation/Foundation.h>

@class Actor;
@class Director;

typedef enum {
    G = 0,
    PG,
    PG13,
    R,
    NC17
} FilmRating;

NS_ASSUME_NONNULL_BEGIN

@interface Film : NSObject {
}

@property (nonatomic) FilmRating filmRating;
@property (nonatomic, weak) NSArray *languages;
@property (nonatomic, weak) NSDate *releaseDate;
@property (nonatomic, strong) NSArray<Actor *> *cast;

- (id)initWithData:(NSDictionary *)data;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) Director *director;
@property (nonatomic) double rating;
@property (nonatomic) BOOL nominated;

@end

NS_ASSUME_NONNULL_END
