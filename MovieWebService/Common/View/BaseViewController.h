//
//  BaseViewController.h
//  MovieWebService
//
//  Created by Chung on 31/03/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
