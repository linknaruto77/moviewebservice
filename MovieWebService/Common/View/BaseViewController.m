//
//  BaseViewController.m
//  MovieWebService
//
//  Created by Chung on 31/03/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

@end
