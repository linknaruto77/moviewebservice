//
//  ListPresenterTests.m
//  MovieWebService
//
//  Created by Chung on 04/04/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ListProtocols.h"
#import "ListPresenter.h"
#import "ListViewController.h"

@interface ListPresenterTests : XCTestCase <ListModuleInterface, ListInteractorOutputProtocol>

@property (nonatomic, strong) ListViewController *vc;
@property (nonatomic, strong) ListPresenter *presenter;
@property (nonatomic, strong) id<ListInteractorInputProtocol> interactor;
@property (nonatomic, strong) NSArray *filmList;

@end

@implementation ListPresenterTests

- (void)setUp {
    [super setUp];
    
    _presenter = [[ListPresenter alloc] init];
    _vc.presenter = _presenter;
    _presenter.interactor = _interactor;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - ListModuleInterface methods

- (NSUInteger)filmCount {
    return _filmList.count;
}

- (Film *)cellInfoAtIndex:(NSInteger)index {
    return _filmList[index];
}

- (void)viewDidLoad {
    [self.interactor retrieveMovieList];
}

- (void)showDetail:(id)film {
    
}

#pragma mark - ListInteractorOutputProtocol methods

- (void)didRetrieveFilms:(NSArray *)films {
    _filmList = films;
}

- (void)onError {
    
}

@end
