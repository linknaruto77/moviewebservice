//
//  ListViewTests.m
//  ListViewTests
//
//  Created by Tan, Michael (Agoda) on 3/10/14.
//  Copyright (c) 2014 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "ListViewController.h"
#import "MovieCell.h"

@interface ListViewTests : XCTestCase
    
@property (nonatomic, strong) ListViewController *vc;

@end

@implementation ListViewTests

- (void)setUp {
    [super setUp];
    _vc = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    [_vc view];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testTableViewIsNotNilAfterViewDidLoad {
    XCTAssertNotNil(_vc.tableView);
}

- (void)testShouldSetTableViewDataSource {
    XCTAssertNotNil(_vc.tableView.dataSource);
}

- (void)testShouldSetTableViewDelegate {
    XCTAssertNotNil(_vc.tableView.delegate);
}

- (void)testConformsToTableViewDataSourceProtocol {
    XCTAssert([_vc conformsToProtocol:@protocol(UITableViewDataSource)]);
    XCTAssert([_vc respondsToSelector:@selector(tableView:numberOfRowsInSection:)]);
    XCTAssert([_vc respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)]);
}

- (void)testConformsToTableViewDelegateProtocol {
    XCTAssert([_vc conformsToProtocol:@protocol(UITableViewDelegate)]);
    XCTAssert([_vc respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]);
}

- (void)testTableViewDidRegisterMovieCell {
    static NSString *CellIdentifier = @"MovieCell";
    MovieCell *cell = [_vc.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    XCTAssertNotNil(cell);
    XCTAssertNotNil(cell.name);
    XCTAssertNotNil(cell.date);
    XCTAssertNotNil(cell.filmRating);
    XCTAssertNotNil(cell.rating);
}

@end
