//
//  ListInteractorTests.m
//  MovieWebService
//
//  Created by Chung on 05/04/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ListProtocols.h"
#import "ListInteractor.h"

@interface ListInteractorTests : XCTestCase <ListInteractorOutputProtocol>

@property (nonatomic, strong) id<ListInteractorInputProtocol> interactor;

@end

@implementation ListInteractorTests

- (void)setUp {
    [super setUp];
    
    ListInteractor* interactor = [[ListInteractor alloc] init];
    interactor.presenter = self;
    _interactor = interactor;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - ListInteractorOutputProtocol methods

- (void)didRetrieveFilms:(NSArray *)films {
    
}

- (void)onError {
    
}

@end
