//
//  DetailViewTests.m
//  DetailViewTests
//
//  Created by Chung on 03/04/2017.
//  Copyright © 2017 Tan, Michael (Agoda). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "DetailViewController.h"
#import "DirectorCell.h"
#import "CastCell.h"

@interface DetailViewTests : XCTestCase {
    DetailViewController *vc;
}

@end

@implementation DetailViewTests

- (void)setUp {
    [super setUp];
    vc = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
    UIView *view = vc.view;
    NSLog(@"%@", view);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testTableViewIsNotNilAfterViewDidLoad {
    XCTAssertNotNil(vc.tableView);
}

- (void)testShouldSetTableViewDataSource {
    XCTAssertNotNil(vc.tableView.dataSource);
}

- (void)testShouldSetTableViewDelegate {
    XCTAssertNotNil(vc.tableView.delegate);
}

- (void)testConformsToTableViewDataSourceProtocol {
    XCTAssert([vc conformsToProtocol:@protocol(UITableViewDataSource)]);
    XCTAssert([vc respondsToSelector:@selector(tableView:numberOfRowsInSection:)]);
    XCTAssert([vc respondsToSelector:@selector(tableView:cellForRowAtIndexPath:)]);
}

- (void)testConformsToTableViewDelegateProtocol {
    XCTAssert([vc conformsToProtocol:@protocol(UITableViewDelegate)]);
    XCTAssert([vc respondsToSelector:@selector(tableView:heightForRowAtIndexPath:)]);
    XCTAssert([vc respondsToSelector:@selector(tableView:heightForHeaderInSection:)]);
    XCTAssert([vc respondsToSelector:@selector(tableView:viewForHeaderInSection:)]);
}

- (void)testTableViewDidRegisterDirectorCell {
    static NSString *CellIdentifier = @"DirectorCell";
    DirectorCell *cell = [vc.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    XCTAssertNotNil(cell);
    XCTAssertNotNil(cell.name);
}

- (void)testTableViewDidRegisterCastCell {
    static NSString *CellIdentifier = @"CastCell";
    CastCell *cell = [vc.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    XCTAssertNotNil(cell);
    XCTAssertNotNil(cell.name);
    XCTAssertNotNil(cell.screeName);
}

@end
